Simple weather app with minimum UI style using https://www.metaweather.com/api/ as weather API

Installation
1. Download / clone repo
2. insert command 
```
    composer install
```
3. edit .env, setting database
```
    DB_DATABASE=$database_name
    DB_USERNAME=$database_username
    DB_PASSWORD=$databse_password
```
4. insert command to migrate city table 
```
    php artisan migrate
```
5. run on default port 8000
```
    php artisan serve 
```
