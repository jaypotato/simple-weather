<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $weather = NULL;
    $saved_city = App\City::all();
    if(empty($saved_city)) $saved_city = NULL;
    return view('home', compact('weather', 'saved_city'));
});

Route::get('current-weather', 'WeatherController@getWeatherByCurrentLocation')->name('weather.current');
Route::get('search', 'WeatherController@getWeatherBySearch')->name('weather.search');
Route::get('save/{city}', 'WeatherController@saveSearch')->name('save.search');