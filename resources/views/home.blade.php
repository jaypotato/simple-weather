<!DOCTYPE html>
<html lang="en">
<head>
  <title>Simple Weather App</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <style>
 
        .wind .dir {
        display:inline-block;
        width:1em;
        height:1em;
        background:url('https://www.metaweather.com/static/img/windarrow.svg');
        background-size: 100% 100%;
        }
        .wind .dir-nne {transform: rotate(22.5deg)}
        .wind .dir-ne {transform: rotate(45deg)}
        .wind .dir-ene {transform: rotate(67.5deg)}
        .wind .dir-e {transform: rotate(90deg)}
        .wind .dir-ese {transform: rotate(112.5deg)}
        .wind .dir-se {transform: rotate(135deg)}
        .wind .dir-sse {transform: rotate(157.5deg)}
        .wind .dir-s {transform: rotate(180deg)}
        .wind .dir-ssw {transform: rotate(202.5deg)}
        .wind .dir-sw {transform: rotate(225deg)}
        .wind .dir-wsw {transform: rotate(247.5deg)}
        .wind .dir-w {transform: rotate(270deg)}
        .wind .dir-wnw {transform: rotate(292.5deg)}
        .wind .dir-nw {transform: rotate(315deg)}
        .wind .dir-nnw {transform: rotate(337.5deg)}
        .mid-bar{margin: 0 auto; padding-top: 1%;}
    </style>
</head>
<body>
    <!-- <script type="text/javascript">
        function geoFindMe() {
        var output = document.getElementById("out");

        if (!navigator.geolocation){
            output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
            return;
        }

        function success(position) {
            var latitude  = position.coords.latitude;
            var longitude = position.coords.longitude;
            $.ajax({
                type: 'GET',
                url: '/current-weather',
                data: {'latitude': latitude, 'longitude' : longitude},
            });
        }

        function error() {
            output.innerHTML = "Unable to retrieve your location";
        }

        navigator.geolocation.getCurrentPosition(success, error);
        }
        geoFindMe();
    </script> -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col">
                    <form method="get" action="{{route('weather.search')}}">
                        <div class="input-group col-md-4 mid-bar">
                            <input class="form-control py-2" name="city" type="search" placeholder="Search City" id="example-search-input">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <form method="get" action="{{route('weather.search')}}">
                        <div class="form-group col-md-4 mid-bar">
                            <label for="city">Search Using Saved Cities</label>
                            <select class="form-control" id="city" name="city" type="submit">
                                <option selected="selected">-- Saved Cities -- </option>
                                @foreach($saved_city as $city) 
                                <option value="{{$city->city_name}}">{{$city->city_name}}</option>
                                @endforeach
                            </select>
                            <span>
                                <button class="btn btn-outline-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="row">
                    @if($weather != NULL)
                    <div class="col-md-6 mid-bar">
                        <dl class="mid-bar">
                                <dt>
                                    {{date('D', strtotime($weather->consolidated_weather[0]->applicable_date))}}, {{$weather->title}}
                                </dt>
                        <dl>                            
                        <div class="col-md-12 mid-bar">
                            <img alt={{$weather->consolidated_weather[0]->weather_state_name}} src={{'https://www.metaweather.com/static/img/weather/'.$weather->consolidated_weather[0]->weather_state_abbr.'.svg'}} class="rounded-circle" style="width:100%;height:auto;max-width:400px; display:block; margin:auto;" />
                            <dl>
                                <dt>
                                    Current {{round($weather->consolidated_weather[0]->the_temp)}}
                                </dt>
                                <dd>
                                    Min {{round($weather->consolidated_weather[0]->min_temp)}} / Max {{round($weather->consolidated_weather[0]->max_temp)}}
                                </dd>
                                <dd class="wind">
                                    <span class="dir dir-{{strtolower($weather->consolidated_weather[0]->wind_direction_compass)}}"></span>
                                    Wind {{round($weather->consolidated_weather[0]->wind_speed)}} Mph / {{round($weather->consolidated_weather[0]->wind_speed * 1.60934)}} Kmh 
                                </dd>
                                <dt>
                                    {{$weather->consolidated_weather[0]->weather_state_name}}
                                </dt>
                                
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col mid-bar" style="text-align: center; padding-bottom:2%;">
                        <a href="{{url('/save/'.$weather->title)}}" class="btn btn-primary" name="" style="display: inline-block;"> Save Search</a>
                    </div>  
                </div>
                <div class="row">
                @for($i=1; $i<6; $i++)
                    <div class="col">
                        <div class="card">
                            <h5 class="card-header">
                                {{date('D', strtotime($weather->consolidated_weather[$i]->applicable_date))}}
                            </h5>
                            <div class="card-body">
                                <img alt={{$weather->consolidated_weather[$i]->weather_state_name}} src={{'https://www.metaweather.com/static/img/weather/'.$weather->consolidated_weather[$i]->weather_state_abbr.'.svg'}} class="rounded-circle" style="width:100%;height:auto;max-width:400px" />
                               <dl>
                               <dd>
                                    Current {{round($weather->consolidated_weather[$i]->the_temp)}}
                               </dd>
                               <dd>
                               Min {{round($weather->consolidated_weather[$i]->min_temp)}} / Max {{round($weather->consolidated_weather[$i]->max_temp)}}
                               </dd>
                               </dl>
                            </div>
                            <div class="card-footer">
                                {{$weather->consolidated_weather[$i]->weather_state_name}}
                            </div>
                        </div>
                    </div>
                @endfor
                </div>
                
                @endif
            </div>
        </div>
    </div>
</body>
</html>
