<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use \Torann\GeoIP\Facades\GeoIP;

class WeatherController extends Controller
{
    public function getWeatherBySearch(Request $request) {
        $location_data = $this->http_request('https://www.metaweather.com/api/location/search/?query='.$request->city);
        $saved_city = City::all();
        if(empty($saved_city)) $saved_city = NULL;
        if($location_data != false) {
            $weather = $this->http_request('https://www.metaweather.com/api/location/'.$location_data[0]->woeid.'/');
            if($weather != false) {
                return view('home', compact('weather', 'saved_city'));
            } else {
                return "something wrong.";
            }
        } else {
            return "city not found";
        }
    }

    public function saveSearch($city) {
        $is_exist = City::whereRaw('lower(city_name) like "%'. strtolower($city). '%"')->get();
        // $is_exist = City::where('city_name', 'ilike', $city)->get();
        if($is_exist->isEmpty()) {
            $c = new City();
            $c->city_name  = $city;
            $c->save();
        }
        $saved_city = City::all();
        if(empty($saved_city)) $saved_city = NULL;
        $weather = NULL;
        return view('home', compact('weather', 'saved_city'));
    }

    public function getWeatherByCurrentLocation(Request $request) {
        dd($request);
    }

    protected function http_request($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $decoded_data = json_decode($data);
        return ($httpcode>=200 && $httpcode<400 && empty($decoded_data) != true) ? $decoded_data : false;
    }
}
